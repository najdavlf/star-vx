# Star-VoXel

Star-VX is C library whose purpose is to manage volume elements, named voxels,
structured as a set of axis aligned cuboids. This library does not know
anything about the volumic data that it handles: it only provides data
structures that partition voxels according to user criteria. It also implements
efficient ways to index voxels into the space partitioning data structures or
to access them through ray-tracing.

Star-VX proposes 2 hierarchical data structures: the *octree* and the *binary
tree*. The first one is used to structure regular 3D data while the second one
is for 1D data. Star-VX build the required data structure following a
bottom-top strategy: the user submits the set of data to partition as well as
the policy used to define when voxels can be merged and the merge operator
itself. The way data are accessed through *indexing* or *ray-tracing* can also be
finely tuned by the user. In addition of the probe position or the ray to
trace, one can provide callbacks to stop traversal at a hierarchy level that is
not a leaf, perform computations at the traversed leaf, discard leafs, etc.
This gives a full control of the host application on the data structure while
its memory representation and its accessors are still fully managed internally
by Star-VX.

## How to build

Star-VX relies on the [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) packages to build.  It also depends
on the [RSys](https://gitlab.com/vaplv/rsys/), library. It is compatible
GNU/Linux as well as Microsoft Windows 7 and later, both in 64-bits on x86-64
architectures. It was successfully built with the [GNU Compiler
Collection](https://gcc.gnu.org) (versions 4.8.5 and later) as well as with
Microsoft Visual Studio 2015.

First ensure that CMake is installed on your system. Then install the RCMake
package as well as the aforementioned prerequisites. Finally generate the
project from the `cmake/CMakeLists.txt` file by appending to the
`CMAKE_PREFIX_PATH` variable the install directories of its dependencies. The
resulting project can be edited, built, tested and installed as any CMake
project. Refer to the [CMake](https://cmake.org/documentation) for further
informations on CMake.

## Release notes

### Version 0.2.1

Sets the CMake minimum version to 3.1: since CMake 3.20, version 2.8 has become
obsolete.

### Version 0.2

- Update the profile of the functors invoked on node traversal: the ray
  origin, direction and range are now provided as input arguments.
- Force the invocation of the challenge and filter functors on the root node.
  Previously these functors were not called on the root;the challenging and
  filtering was starting on its children.

### Version 0.1

- Add the `svx_tree_write` and the `svx_tree_create_from_stream` functions used
  to serialize and de-serialize the tree data structure, respectively.

## License

Copyright (C) 2018, 2020, 2021 [|Meso|Star>](https://www.meso-star.com)
(<contact@meso-star.com>). Copyright (C) 2018 Université Paul Sabatier
(<contact-edstar@laplace.univ-tlse.fr>). Star-VoXel is free software released
under the GPL v3+ license: GNU GPL version 3 or later. You are welcome to
redistribute it under certain conditions; refer to the COPYING file for
details.
